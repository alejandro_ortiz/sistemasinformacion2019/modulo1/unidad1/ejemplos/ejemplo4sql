﻿-- Base de datos ejemplar-socio opcion N-1 con 2 tablas

  DROP DATABASE IF EXISTS opcion5;
  CREATE DATABASE opcion5;
  USE opcion5;

-- creacion de tablas

  CREATE TABLE socio(
    cod_socio int AUTO_INCREMENT,
    PRIMARY KEY (cod_socio)
  );

  CREATE TABLE ejemplar(
    cod_ejemplar int AUTO_INCREMENT,
    socio int,
    fecha_i date,
    fecha_f date,
    PRIMARY KEY (cod_ejemplar),
    CONSTRAINT fkEjemplarSocio FOREIGN KEY (socio) REFERENCES socio(cod_socio) 
  );