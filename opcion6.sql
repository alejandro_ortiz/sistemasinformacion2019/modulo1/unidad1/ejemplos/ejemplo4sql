﻿-- Base de datos ejemplar-socio opcion 1-N con 2 tablas

  DROP DATABASE IF EXISTS opcion6;
  CREATE DATABASE opcion6;
  USE opcion6;

-- creacion de tablas

  CREATE TABLE ejemplar(
    cod_ejemplar int AUTO_INCREMENT,
    PRIMARY KEY (cod_ejemplar)
  );

  CREATE TABLE socio(
    cod_socio int AUTO_INCREMENT,
    ejemplar int,
    fecha_i date,
    fecha_f date,
    PRIMARY KEY (cod_socio),
    CONSTRAINT fkSocioEjemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar(cod_ejemplar) 
  );