﻿-- Base de datos ejemplar-socio opcion N-N

  DROP DATABASE IF EXISTS opcion1;
  CREATE DATABASE opcion1;
  USE opcion1;

-- creacion de tablas

  /*
    crea o reemplaza la tabla si ya esta creada 
    CREATE OR REPLACE TABLE ejemplar(
  */
  CREATE TABLE ejemplar(
    cod_ejemplar int AUTO_INCREMENT,
    PRIMARY KEY (cod_ejemplar)
  );

  CREATE TABLE socio(
    cod_socio int AUTO_INCREMENT,
    PRIMARY KEY (cod_socio)
  );

  CREATE TABLE presta(
    ejemplar int,
    socio int,
    fecha_i date,
    fecha_f date,
    PRIMARY KEY (ejemplar, socio),
    CONSTRAINT fkPrestaEjemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar(cod_ejemplar),
    CONSTRAINT fkPrestaSocio FOREIGN KEY (socio) REFERENCES socio(cod_socio) 
  ); 