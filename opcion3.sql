﻿-- Base de datos ejemplar-socio opcion N-1

  DROP DATABASE IF EXISTS opcion3;
  CREATE DATABASE opcion3;
  USE opcion3;

-- creacion de tablas

  CREATE TABLE ejemplar(
    cod_ejemplar int AUTO_INCREMENT,
    PRIMARY KEY (cod_ejemplar)
  );

  CREATE TABLE socio(
    cod_socio int AUTO_INCREMENT,
    PRIMARY KEY (cod_socio)
  );

  CREATE TABLE presta(
    ejemplar int,
    socio int,
    fecha_i date,
    fecha_f date,
    PRIMARY KEY (ejemplar, socio),
    UNIQUE KEY (ejemplar),
    CONSTRAINT fkPrestaEjemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar(cod_ejemplar),
    CONSTRAINT fkPrestaSocio FOREIGN KEY (socio) REFERENCES socio(cod_socio) 
  );